package com.albeip;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CuentaBancaria {
    private String nombreCliente;
    private String numeroCuenta;
    private double saldo;
    private List<String> movimientos;

    public CuentaBancaria(String nombreCliente, String numeroCuenta, double saldo) {
        if (nombreCliente==null || nombreCliente.trim().length()==0)
            throw new RuntimeException("Cadena vacía");
        if (numeroCuenta==null || numeroCuenta.trim().length()==0)
            throw new RuntimeException("Cuenta no válida");
        this.nombreCliente = nombreCliente;
        this.numeroCuenta = numeroCuenta;
        this.saldo = saldo;
        this.movimientos = iniciaMovimientos(saldo);
    }

    public static CuentaBancaria createInstance(String nombreCliente, String numeroCuenta, double saldo) {
        return new CuentaBancaria(nombreCliente, numeroCuenta, saldo);
    }

    public String getNombreCliente() {
        return nombreCliente;
    }


    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public double getSaldo() {
        movimientos.add("");
        return saldo;
    }


    public void asignarSaldoCuenta(double cantidad) {
        if (cantidad < 0)
            throw new RuntimeException("Cantidad negativa no es válida");
        saldo += cantidad;
        creaMovimiento(saldo);
    }

    public void retiro(double cantidad) {
        if (cantidad > saldo)
            throw new RuntimeException("El monto a retirar excede el saldo de la cuenta");
        saldo -= cantidad;
        creaMovimiento(saldo);
    }

    private List<String> iniciaMovimientos(double saldo) {
        List<String> movimientos = new ArrayList<>();
        movimientos.add("FECHA\t\t\t\tSALDO");
        movimientos.add(creaFechaActual() + "\t\t\t\t" + saldo);
        return movimientos;
    }

    private void creaMovimiento(double saldo) {
        movimientos.add(creaFechaActual() + "\t\t\t\t" + saldo);
    }

    private String creaFechaActual() {
        return (new Date()).toString();
    }

    public void imprimeMovimientos() {
        for (String movimiento: movimientos)
            System.out.println(movimiento);
    }


    @Override
    public String toString() {
        return "\nCuentaBancaria{" +
                "nombreCliente='" + nombreCliente + '\'' +
                ", numeroCuenta='" + numeroCuenta + '\'' +
                '}';
    }
}
